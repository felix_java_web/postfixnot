package au.com.specsheet;

import java.io.IOException;

public class Main {


    public static void main(String[] args) throws IOException {
        // Input file which needs to be parsed
        String csvInputFile = null;
        if (args.length > 0) {
            csvInputFile = args[0];
        }
        if (csvInputFile == null || !(Utils.isPathValid(csvInputFile)) || !(Utils.isExtensionCsv(csvInputFile))) {
            throw new IllegalArgumentException("CSV file name in argument was not submitted or does not exist, proper csv file name format is ----name----.csv");
        }
        CsvToMatrix csvToMatrix = new CsvToMatrix();
        //outer list is Raws and inner list is Cols
        var parsedCsvRowsCols = csvToMatrix.parseCsvToMatrixList(csvInputFile);
        // I will be a raw number
        for (int i = 0; i < parsedCsvRowsCols.size(); i++) {
            // J will be a col number of raw I
            for (int j = 0; j < parsedCsvRowsCols.get(i).size(); j++) {
                csvToMatrix.parseMainMatrixForPostfixNotation(parsedCsvRowsCols.get(i).get(j), i, j, parsedCsvRowsCols);
            }
        }
        csvToMatrix.outputMatrixListToCsvStdout(parsedCsvRowsCols);

    }
}

