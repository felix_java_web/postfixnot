package au.com.specsheet;

import java.io.File;

public class Utils {

    private Utils() {
    }

    //make sure file is physically exists
    public static boolean isPathValid(String filename) {
        File f = new File(filename);
        return (f.exists() && !f.isDirectory()) ;
    }

    //make sure file has csv extension
    public static boolean isExtensionCsv(String filename) {
        return filename.substring(filename.lastIndexOf(".") + 1).equalsIgnoreCase("csv");
    }

    //Convert column names to numbers
    public static int getColumnIndexNumberToExcelColumn(String excelColumn) {
        var excelColumnUpperCase = excelColumn.toUpperCase();
        var base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int i = 0, j = 0, result = 0;
        for (i = 0, j = excelColumnUpperCase.length() - 1; i < excelColumnUpperCase.length(); i += 1, j -= 1) {
            result += Math.pow(base.length(), j) * (base.indexOf(excelColumnUpperCase.charAt(i) + 1));
        }
        return result;
    }

    public static String fmt(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }

}
