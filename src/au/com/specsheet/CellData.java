package au.com.specsheet;


//I like to use TUPLEs like double triple quadruple sometimes -) but no library allowed so this is mi kinda tuple
public class CellData {

    public CellData(String csvValue) {
        this.csvValue = csvValue.trim().toUpperCase();
    }

    public String csvValue;
    public double doubleValue;
    public String stringValue = "#ERR";
    public boolean calculated = false;

    public String getCsvValue() {
        return csvValue;
    }

    public CellData setCsvValue(String csvValue) {
        this.csvValue = csvValue.toUpperCase();
        return this;
    }

    public CellData setError() {
        this.calculated = true;
        return this;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public CellData setDoubleValue(double doubleValue) {
        this.doubleValue = doubleValue;
        return this;
    }

    public String getStringValue() {
        return stringValue;
    }

    public String getCalcedOrInitialValue() {
        return calculated ? stringValue : csvValue;
    }

    public CellData setStringValue(String stringValue) {
        this.stringValue = stringValue;
        return this;
    }

    public boolean isCalculated() {
        return calculated;
    }

    public CellData setCalculated(boolean calculated) {
        this.calculated = calculated;
        return this;
    }

    @Override
    public String toString() {
        return "CellData{" +
                "csvValue='" + csvValue + '\'' +
                ", doubleValue=" + doubleValue +
                ", stringValue='" + stringValue + '\'' +
                ", calculated=" + calculated +
                '}';
    }

    public void setCalcValue(String finalResult) {
        this.calculated = true;
        if (!finalResult.equals("#ERR")) {
            this.setDoubleValue(Double.valueOf(finalResult));
            this.setStringValue(finalResult);
        }
    }
}
