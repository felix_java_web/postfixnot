package au.com.specsheet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class CsvToMatrix {

    public static final String DIGIT_DOUBLE_INT_PATTERN = "^-?[0-9]+([\\,\\.][0-9]+)?$";
    public static final String CELL_PATTERN = "[A-Z]{1}\\d+";
    public static final String SPACE_STRING = " ";
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVISION = "/";
    public static final String ERROR_STRING = "#ERR";
    public static final String COMMA_STRING = ",";
    public static final String CELL_ADDRESS_REGEX = "(?<=\\D)(?=\\d)";
    public static final String EMPTY_STRING = "";

    public void outputMatrixListToCsvStdout(List<List<CellData>> parsedCsvRowsCols) throws IOException {
        // I will be a raw number
        for (int i = 0; i < parsedCsvRowsCols.size(); i++) {
            // J will be a col number of raw I
            StringBuilder csvLine = new StringBuilder(EMPTY_STRING);
            csvLine.append( parsedCsvRowsCols.get(i).stream().map( cellData -> cellData.getStringValue() ).collect(Collectors.joining(COMMA_STRING)) );
            System.out.println(csvLine);
        }
    }

    public void parseMainMatrixForPostfixNotation(CellData cellData, int rowNum, int columnNum, List<List<CellData>> csvRawsColsMatrix) {
        if (cellData.getCsvValue() == null || cellData.getCsvValue().trim().length() == 0) {
            cellData.setError();
            return;
        }
        //keep in mind that rawNum and colNum are indexes of raws and cols -1 - as arraylist starts from 0
        Stack<String> operands = new Stack<>();
        for (String str : cellData.getCsvValue().trim().split(SPACE_STRING)) {
            if (str.trim().equals(EMPTY_STRING)) {
                continue;
            }

            switch (str) {
                case PLUS:
                case MINUS:
                case MULTIPLY:
                case DIVISION:
                    if (operands.size() < 2) {
                        cellData.setError();
                        return;
                    }
                    // OK here is the most interesting
                    // we got operator which means we must obtain 2 values from stack
                    String rightStr = operands.pop();
                    String leftStr = operands.pop();
                    //if any of them are cell name or digital value - we just return error
                    if ((rightStr.matches(DIGIT_DOUBLE_INT_PATTERN) || rightStr.matches(CELL_PATTERN)) && (leftStr.matches(DIGIT_DOUBLE_INT_PATTERN) || leftStr.matches(CELL_PATTERN))) {
                        rightStr = getStringFromOtherCellUntilItsErrorOrValueRecursive(csvRawsColsMatrix, rightStr);
                        leftStr = getStringFromOtherCellUntilItsErrorOrValueRecursive(csvRawsColsMatrix, leftStr);
                        if (rightStr.equals(ERROR_STRING) || leftStr.equals(ERROR_STRING)) {
                            cellData.setError();
                            return;
                        }
                    } else {
                        cellData.setError();
                        return;
                    }
                    //now we clearly have doubles in rightStr and leftStr
                    doThePostfixNotationAndPushResultToStack(operands, str, rightStr, leftStr);
                    break;
                default:
                    operands.push(str);
                    break;
            }
        }
        //before return make sure all left in stack is one item
        String finalResult = operands.pop();
        if (operands.size() == 0 && (finalResult.matches(DIGIT_DOUBLE_INT_PATTERN) || (finalResult.matches(CELL_PATTERN)))) {
            cellData.setCalcValue(getStringFromOtherCellUntilItsErrorOrValueRecursive(csvRawsColsMatrix, finalResult));
        } else {
            cellData.setError();
        }
    }

    private void doThePostfixNotationAndPushResultToStack(Stack<String> operands, String str, String rightStr, String leftStr) {
        //it is clearly a digit now in right and left Strs
        double right = Double.parseDouble(rightStr);
        double left = Double.parseDouble(leftStr);
        double value = 0;
        switch (str) {
            case PLUS:
                value = left + right;
                break;
            case MINUS:
                value = left - right;
                break;
            case MULTIPLY:
                value = left * right;
                break;
            case DIVISION:
                if (right == 0) {
                    value = 0;
                } else {
                    value = left / right;

                }
                break;
            default:
                break;
        }
        operands.push(Utils.fmt(value));
    }

    private String getStringFromOtherCellUntilItsErrorOrValueRecursive(List<List<CellData>> csvRawsColsMatrix, String finalValue) {
        if (finalValue.matches(CELL_PATTERN)) {
            //TODO Ideally check raw columns are accessible in Matrix to avoid outofbounds exception -
            int column = Utils.getColumnIndexNumberToExcelColumn(finalValue.split(CELL_ADDRESS_REGEX)[0]);
            int raw = Integer.parseInt(finalValue.split(CELL_ADDRESS_REGEX)[1]);
            String newValue = csvRawsColsMatrix.get(raw - 1).get(column - 1).getCalcedOrInitialValue();
            if (newValue.equals(finalValue)) {
                //cell has a reference to itself it is an error
                return ERROR_STRING;
            }
            finalValue = getStringFromOtherCellUntilItsErrorOrValueRecursive(csvRawsColsMatrix, csvRawsColsMatrix.get(raw - 1).get(column - 1).getCalcedOrInitialValue());
        }
        return finalValue;
    }

    public List<List<CellData>> parseCsvToMatrixList(String csvInputFile) throws IOException {
        List<List<CellData>> matrixListFromCsv = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvInputFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                List<CellData> cellDataList = new ArrayList<>();
                matrixListFromCsv.add(cellDataList);
                Arrays.asList(line.split(COMMA_STRING)).forEach(csvCellValue -> cellDataList.add(new CellData(csvCellValue)));
            }
        }
        return matrixListFromCsv;
    }

}
